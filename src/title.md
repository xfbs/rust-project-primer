
```admonish
This is a prerelease version of this book. Feel free to check if out!
I would greatly appreciate it if you left me some [feedback](https://forms.gle/XA5ejDUDoQoxbAvJ8).
If something is wrong, feel free to leave a merge request on the [repository](https://gitlab.com/rust-project-primer/book)
```

<center>

# Rust Project Primer

## A Practical Guide on how to Structure and Maintain your Rust Projects

by

*Patrick M. Elsen*

CC BY-NC-SA 4.0 Licensed

</center>
