# Privacy

This book is statically hosted by [GitLab Pages][gl-pages], therefore their
privacy policy applies.

To get some insight into how many people use the book, and which pages they
visit, this book uses privacy-perserving analytics provided by
[Plausible][plausible]. They use servers located in the EU, are GRPD-compliant
and collects only anonymized information (no persistent tracking, no cookies).
Because I believe in data transparency, I am making this data available
[here][analytics].

By using this website, you agree to these data policies. If you do not like
them, feel free to use an adblocker (such as [uBlock Origin][ublock], which
will block Plausible. You may also print and use a PDF version of this book, or
clone the repository and build and view the book locally.

[gl-pages]: https://about.gitlab.com/privacy/
[ublock]: https://en.wikipedia.org/wiki/UBlock_Origin
[plausible]: https://plausible.io/data-policy
[analytics]: https://plausible.io/rustprojectprimer.com
