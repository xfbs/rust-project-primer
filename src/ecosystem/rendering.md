# Game Development

Game Development often requires one to write code that performs relatively well,
because even small latencies are noticeable to end-users. Game engines have to be
able to track and update a relatively complex world, run physics simulations, run
game logic, and render the world in 2D or 3D. 

[Are We Game Yet](https://arewegameyet.rs/) tracks the progress of the Rust ecosystem
around game development. But as of writing, there are two game engines that have received
some amonut of popularity.

## Bevy

[Bevy](https://bevyengine.org/)

*TODO*

## Fyrox

[Fyrox](https://fyrox.rs/)

*TODO*
