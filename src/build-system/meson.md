# Meson

Meson is a bit of an oddball to include here. It does not offer any of the
interesting features that other build systems do, for example the ability to
easily cache build artifacts.

The reason I am including it here is because it has some features that make
it useful for the niche use-case of building GTK or FlatPak applications, which
it has built-in support for. This is why you see a lot of GNOME developers that
use Rust use it as their primary build system.

## Reading

TODO
