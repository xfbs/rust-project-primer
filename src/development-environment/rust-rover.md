# RustRover

[RustRover][rustrover] is a commercial IDE offered by JetBrains. It has a
deeper integration and more intelligent features than the other IDEs listed
here, but is only free for personal use.

![](https://www.jetbrains.com/rust/img/overview/Work-in-a-team.png)

It is being actively developed, and new features that make writing Rust code
and managing Rust projects are constantly added. The advantage is that it is
all integrated and works out-of-the-box, unlike Visual Studio Code which needs
some custom plugins that achieve what it can do.

The only downside of it is that it is commercial, meaning that it is not
open-source.

[rustrover]: https://www.jetbrains.com/rust/
