# Visual Studio Code

* screenshot of vscode (light/dark mode)

Visual Studio Code is a clone of the previously popular Atom editor that is sponsored by
Microsoft. Compared to Visual Studio, it is lightweight and relatively fast, and has the
advantage of being easily extensible. It has a vast ecosystem of plugins for various
programming languages, including Rust.

## Plugins

### rust-analyzer







https://code.visualstudio.com/docs/languages/rust


