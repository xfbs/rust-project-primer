# Changelog

When it comes to creating releases, an important aspect of doing so is
communicating the changes that have occured in the new version. This is
true regardless of whether your Rust project is a library or whether it
is a product.

In the open-source world, this is typically done using a *changelog*,
which is typically simply a file checked into the repository that keeps
a list of the important changes for downstream users of the software
for every version that is released.

## Reading

[Keep A Changelog](https://keepachangelog.com/en/1.1.0/) by Olivier Lacan

*Keep A Changelog is a specification for how to structure changelogs. It
attempts to standardize their structure and make them useful, and explains why
they are useful.*
