# Examples

There are some things that I consider to be part of *documentation* even
though technically, they are not documentation. Those two are *unit tests*
and *examples*.

## Reading

[Add examples to your Rust libraries](http://xion.io/post/code/rust-examples.html) by Karol Kuczmarski

*Karol explains the need work working examples when using an unfamiliar
library, and how Rust supports this out-of-the-box with its support for
examples. She explains that Rust treats examples as documentation, and builds
them when you run `cargo test`. She argues that all Rust projects should come
with good examples, because they make using the code easier and help people get
started.*
