# Design



## Reading

https://ntietz.com/blog/reasons-to-write-design-docs/

https://dzone.com/articles/how-to-write-rfcs-for-open-source-projects

https://opensource.com/article/17/9/6-lessons-rfcs

https://rust-lang.github.io/rfcs/

https://philcalcado.com/2018/11/19/a_structured_rfc_process.html

https://adr.github.io/

https://cloud.google.com/architecture/architecture-decision-records

https://github.com/joelparkerhenderson/architecture-decision-record

https://docs.aws.amazon.com/prescriptive-guidance/latest/architectural-decision-records/adr-process.html

https://learn.microsoft.com/en-us/azure/well-architected/architect-role/architecture-decision-record
