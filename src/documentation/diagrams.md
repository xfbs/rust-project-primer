# Diagrams

There are some useful tools that can be used to draw such diagrams:

- TODO: show how to include in rustdoc/mdbook

## draw.io

[draw.io](https://draw.io) is a web-application that lets you draw diagrams.
All of the diagrams in this book are made with it.

### Examples

- [draw.io example diagrams](https://www.drawio.com/example-diagrams)
- [Layered architecture diagrams with Draw.io](https://blogs.oracle.com/cloud-infrastructure/post/layered-architecture-diagrams-drawio)

## Excalidraw

- [Excalidraw](https://excalidraw.com/)

## PlantUML

- [PlantUML](https://www.plantuml.com/)

## Mermaid

- [Mermaid](https://mermaid.js.org/)

- https://brycemecum.com/2023/03/31/til-mermaid-tracing/

## Reading
