# Tools

This chapter showcases some tools the Rust community has come up with that can
help you in maintaining, navigating or developing Rust software projects.

## Readme

[Rust Tooling: 8 tools that will increase your productivity](https://www.shuttle.rs/blog/2024/02/15/best-rust-tooling) by Joshua Mo

*Joshua showcases and explains some tools for Rust developers that can increase
your productivity, and gives examples for how they can be used.*

[Awesome Rust Tools](https://github.com/unpluggedcoder/awesome-rust-tools)

*This is a list of awesome tools written in Rust. It showcases tools in various
categories, from general-purpose command-line tools to tools specifically for
Rust development, maintenance or navigation.*

[Cargo plugins](https://lib.rs/development-tools/cargo-plugins)

*This is a list of useful plugins for Cargo, sorted by their popularity (as
measured by the download count from the Rust crates registry).*
