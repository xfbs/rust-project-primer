# Rust Project Primer

*A Practical Guide on how to Structure and Maintain your Rust Projects*

## Summary

> This guide offers advice on structuring and maintaining Rust projects,
> drawing from popular Rust projects and personal experience. It should not be
> viewed as a definitive guide, but rather as a collection of potential issues
> you might encounter and various ways to address them. Recognizing that some
> problems have multiple effective solutions, this guide presents a range of
> options. The hope is that it equips you with valuable advice, allowing your
> projects to benefit from the learnings of those that came before. 

## License

[CC BY-NC-SA 4.0](LICENSE.md)
